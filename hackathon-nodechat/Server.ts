import * as http from 'http';
import * as socketIO from 'socket.io';
import * as express from 'express';
import * as Collections from "typescript-collections";
import * as BodyParse from "body-parser";

class Server {
    public static readonly PORT: number = 8080;
    private app: express.Application;
    private server: http.Server;
    private io: socketIO.Server;
    private port: string | number;



    constructor() {
        this.config();
        this.createApp();
        this.createServer();
        this.socket();
        this.listen();
    }

    private createApp = () => {
        this.app = express();
    };

    private createServer = () => {
        this.server = http.createServer(this.app);
    };

    private config = () => {
        this.port = process.env.PORT || Server.PORT;
    };

    private socket = () => {
      this.io = socketIO(this.server);
    };

    private listen = () => {
      this.server.listen(this.port, this.onListening);
      this.io.on('connection', this.onConnecting);
    };

    private onListening = () => {
        console.log("Running server on port %s", this.port)
    };

    private onConnecting = (socket: socketIO.Socket) => {
        // Init event hook
        let token = socket.handshake.query.token;
        if (token && !User.users.containsKey(token)) {
            let user = new User(token, socket);
            user.sendMsg("Connect success!");
        }
    };
}

class User {
    static users: Collections.Dictionary<string, User> = new Collections.Dictionary<string, User>();

    public readonly uniqueId: string;
    private socket: socketIO.Socket;

    constructor(uniqueId: any, newSocket: socketIO.Socket) {
        this.uniqueId = uniqueId;
        this.socket =  newSocket;
        this.socket.on('message', this.onReceivingMessage);
        this.socket.on('disconnect', this.onDisconnect);

        User.users.setValue(this.uniqueId, this);
    }

    private onDisconnect = () => {
        console.log("[User<%s>] disconnected!", this.uniqueId);
        User.users.remove(this.uniqueId)
    };

    private onReceivingMessage = (message: string) => {
        console.log("[User<%s>]: %s", this.uniqueId, message);
        // Send to HaiVQ's bot entry point
        // TODO
    };

    public sendMsg = (message: string) => {
        this.socket.emit('message', message);
    };

    public sendDMsg = (doctorName: string, message: string) => {
        console.log("[Doctor %s]: %s", doctorName, message);
        let dmsg: DMessage = {
            doctorName: doctorName,
            message: message
        };
        this.socket.emit('dmessage', dmsg);
    }
}

interface DMessage {
    doctorName: string;
    message: string;
}

interface WebhookCall {
    uniqueId: string,
    message: string
}

interface WebhookDCall {
    doctorName: string,
    uniqueId: string,
    message: string
}

class Webhook {
    public static readonly port: number = 8081;
    private app: express.Application;
    private server: http.Server;
    private port: string | number;

    constructor() {
        this.app = express();
        this.server = http.createServer(this.app);
        this.port = process.env.WH_PORT || Webhook.port;

        this.app.use(BodyParse.json());
        this.app.use(BodyParse.urlencoded({extended: true}));

        const whRouter = express.Router();
        whRouter.post("/sendMessage", this.onReceivingMessage);
        whRouter.post("/sendDMessage", this.onReceivingDMessage);

        this.app.use('/wh', whRouter);
        this.server.listen(this.port, this.onListen);
    }

    private onReceivingMessage = (req, res) => {
        // console.log(req.body);
        let call: WebhookCall = req.body as WebhookCall;
        if (User.users.containsKey(call.uniqueId)) {
            User.users.getValue(call.uniqueId).sendMsg(call.message);
            res.status(200);
            res.send("Completed!");
            //Return 200
        } else {
            // Return BadRequest
            res.status(400);
            res.send("BadRequest");
        }
    };

    private onReceivingDMessage = (req, res) => {
        let dCall: WebhookDCall = req.body as WebhookDCall;
        if (User.users.containsKey(dCall.uniqueId)) {
            User.users.getValue(dCall.uniqueId).sendDMsg(dCall.doctorName, dCall.message);
            res.status(200);
            res.send("Completed!");
        } else {
            res.status(400);
            res.send("BadRequest");
        }
    };

    private onListen = () => {
        console.log("Webhook API is listening on %s", this.port);
    }
}

new Server();
new Webhook();