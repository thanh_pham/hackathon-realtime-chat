import * as SocketIO from "socket.io-client";

let socket = SocketIO.connect('http://localhost:8080', {
   query: {token: "TEST_TOKEN123847129384791"}
});

interface DMessage {
    doctorName: string;
    message: string;
}

socket.on('message', (message) => {
    console.log("[SERVER]: %s", message);
});

socket.on('dmessage', (dmsg: DMessage) => {
    console.log("[%s]: %s", dmsg.doctorName, dmsg.message);
});

let stdin = process.openStdin();

stdin.addListener("data", (d) => {
    socket.emit('message', d.toString().trim());
});